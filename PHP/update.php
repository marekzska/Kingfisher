<?php
header('Content-Type:  application/json');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$jsonString = file_get_contents('./objectives.json');
$data = json_decode($jsonString, true);

$data[$_POST['value']]['solved'] = "1";
$data[$_POST['value']+1]['show'] = "1";

$newJsonString = json_encode($data);
file_put_contents('./objectives.json', $newJsonString);
echo json_encode($_POST['value']);
