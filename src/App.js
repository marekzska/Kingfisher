import Intro from "./Intro.js";
import "./App.css";
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <BrowserRouter basename='/Kingfisher'>
    {/* <BrowserRouter> */}
      <Intro />
    </BrowserRouter>
  );
}

export default App;
