import React from "react";
import Objective from "./Objective.js";
import "./objectivelist.css";

export default function ObjectiveList(props) {
  const { objectives } = props;

  return (
    <ul id='objectiveUlList'>
      {Object.entries(objectives).map((item, index) => (
        <Objective
          {...item}
          index={index}
          key={index}
          fetchObjectivesAgain={props.fetchObjectivesAgain}
        />
      ))}
    </ul>
  );
}
