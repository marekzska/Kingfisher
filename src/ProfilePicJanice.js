import React from "react";
import "./profilepic.css";

export default function ProfilePicJanice() {
  return (
    <div id="offset">
      <div id='picOrbitals'>
        <img
          id='profilePic'
          src='./resources/janice.png'
          alt='profilePic'
        />
      </div>
    </div>
  );
}
