import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./dashboard.css";
import CredentialsJanice from "./CredentialsJanice.js";
import ProfilePicJanice from "./ProfilePicJanice.js";

export default function Janice() {
  const [loggedIn, setLoggedIn] = useState(() =>
    localStorage.getItem("isLoggedIn")
  );
  const navigate = useNavigate();

  useEffect(() => {
    localStorage.setItem("isLoggedIn", loggedIn);
  }, [loggedIn]);

  function handleButtonClick() {
    setLoggedIn("false");
    navigate(-1);
  }

  if (loggedIn === "true") {
    return (
      <>
        <div id='centerClass'>
          <div id='agentInfo'>
            <ProfilePicJanice />
            <CredentialsJanice />
          </div>

          <div id='objectiveList'></div>
        </div>

        <button
          title='Log Out'
          onClick={handleButtonClick}
          value='ano'
          id='logout'
          name='ano'
        >
          <img
            id='logOutImg'
            src='https://cdn-icons-png.flaticon.com/512/66/66882.png?w=360'
            alt='log out'
          />
        </button>
      </>
    );
  }
}
