import React from 'react';
import "./credentials.css";

export default function CredentialsJanice(){

    return (
      <ul id='credentials'>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Legal Name:</h3>
          <h3 className='credentialItem'>J.L.</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Alias:</h3>
          <h3 className='credentialItem'>Soothsayer</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Age:</h3>
          <h3 className='credentialItem'>28</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Clearance:</h3>
          <h3 className='credentialItem'>AA - Secret</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Classification:</h3>
          <h3 className='credentialItem'>Liaison Officer</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Deployment:</h3>
          <h3 className='credentialItem'>Offices</h3>
        </li>
        <li className='credentialLine'>
          <h3 className='credentialItem'>Status:</h3>
          <h3 className='credentialItem'>Active</h3>
        </li>
      </ul>
    );
}