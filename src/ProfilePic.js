import React from "react";
import "./profilepic.css";

export default function ProfilePic() {
  return (
    <div id="offset">
      <div id='picOrbitals'>
        <img
          id='profilePic'
          src='./resources/profilePic.jpg'
          alt='profilePic'
        />
      </div>
    </div>
  );
}
