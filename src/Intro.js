import React from "react";
import "./intro.css";
import AgentForm from "./AgentForm.js";
import Dashboard from "./Dashboard.js";
import Janice from "./Janice.js";
import { Routes, Route } from "react-router-dom";

export default function Intro() {
  return (
    <>
      <div className='page__style animate_content'>
        <Routes>
          <Route path='/' element={<AgentForm />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/janice' element={<Janice />} />
        </Routes>
      </div>
    </>
  );
}
